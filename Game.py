#Owen Burek
#CMPT 120L 113: Introduction to Programming
#Version 0.7 (November 11, 2016)

from locations import Locations
from items import Items
from player import Player
from limiteditem import LimitedItem

while True:

    mistmanDew=Items("mistman dew","These droplets of viscous liquid seem to bind to your skin.")

    hardWormSkin=Items("hard worm skin","This dried up worm corpse has a very hard outer layer.")

    wormOrgans=Items("worm organs","These slimy organs still pulse with life.")

    wormMap=Items("map","This map has been carved into a rolled out worm skin.")

    wormFluids=Items("worm fluids","This came from within the greater worm. It emanates life itself.")

    wormHeart=LimitedItem("worm heart",
                    "It's still beating. It's arteries and veins, if left unchecked, move towards your chest.")
    
    vortexEntry=Locations("vortexEntry",
                         ("You're floating amidst the vortex you were sucked into. "
                          "\nBeams of light and asteroids (or, at least, that's what they look like) "
                          "\nare whizzing by you, bending and twisting in seemingly impossible ways."),
                          "You return to where you entered the vortex.",
                          [],
                          0)

    cloudSpace=Locations("cloudSpace",
                        ("You push yourself along, until you come across several large clouds of mist. "
                         "\nLike you, they don't seem to be affected by the pull of whatever vortex you've entered. "
                         "\nThe clouds approach and circle you, almost as if they had a will of their own."),
                         "You return to where the lesser mist clouds are.",
                         ["dew"],
                         1)
                         
    wormSpace=Locations("wormSpace",
                       ("You approach an area filled with creatures that make you think of caterpillars. "
                        "\nEach one is fairly long. You watch as they slowly coil themselves like springs."
                        "\nAfter a few more seconds, you see them elongate their bodies in an instant, "
                        "\nlaunching themselves forward towards some distant location. "
                        "\nYou are now alone, left to ponder the purpose of such strange beings."),
                       ("You return to the area where you first encountered the worms."
                        "\nYou wonder what they were looking for in this dark vortex."),
                        [],
                        2)

    morgue=Locations("morgue",
                    ("You arrive and examine the deflated, tube-like objects floating around here. "
                     "\nAfter some examination, you understand that the 'tubes' are actually those "
                     "\nstrange catepillar-like creatures you saw earlier."
                     "\nWith their wilted forms and lack of movement, you assume that they are corpses."
                     "\nAfter some examination, you see that their insides hold no organs."),
                     "You return to the worm graveyard.",
                     ["skin"],
                     3)

    distortedZone=Locations("distortedZone",
                           ("An especially distorted area lies ahead."
                            "\nEverything that touches this area seems to disappear instantly."),
                            "You return to the wall of distortion",
                            ["organs"],
                            4)

    mistLeader=Locations("mistLeader",
                        ("You approach a huge, mist-like entity. Surrounding it are the corpses of those"
                         "\nworms you've seen. It looks like the tube-like creatures' insides are gone."
                         "\nSuddenly, you hear a voice in you head: 'I assume you are wondering why you are here."
                         "\nTo be blunt, your home world no longer exists as you once knew it."
                         "\nWe've consumed it for the purpose of feeding ourselves."
                         "\nOr, rather, the worms are currently consuming it. Without them, "
                         "\nand without inferior lifeforms such as those on your planet, we could not survive.'"
                         "\nThe voice stops, leaving you in despair. You wonder why you're the only one still alive."
                         "\nAfter some time, the voice resumes: 'Do not misunderstand; you are not special. The only"
                         "\nreason you still live is because in order to continue feeding off your planet,"
                         "\nwe needed to use a lifeform's mind as a tether. It just happened to be your mind. "
                         "\nHowever, if you wish, you can become like us. You will live"
                         "\nabove all other existenes in the universe. When you are ready, return here.'"
                         "\nYou stand there, shaken by the truth of your circumstances. Looking around you,"
                         "\nthe black hole seems more menacing."),
                        ("You approach the imposing mist cloud. Its voice permeates your mind:"
                         "\n'It is not yet time. Come back when you are truly prepared.'"),
                         ["map"],
                         5)

    vortexCore=Locations("vortexCore",
                        ("You approach the final destination of all of the matter swirling around you."
                         "\nThe compacted matter at the core of the vortex slowly expands and then quickly shrinks."
                         "\nYou notice the worms you saw earlier, and they seem to be feeding on the mass "
                         "\nbefore you. There are several clouds of mist circling the dense matter."),
                         "You return to the black hole's center. The size of the mass is about the same.",
                         [],
                         6)

    innerVortex=Locations("innerVortex",
                         ("Another smaller vortex lies before you. As you enter, you are met with numerous wriggling,"
                          "\nviscous orbs. Upon closer inspection, you see that inside the orbs are small worms."
                          "\nYou hear something very large moving deeper within."),
                         ("You return to the worm incubation vortex. "
                          "\nThe worms are still wriggling inside their 'eggs.'"),
                          [],
                          7)

    wormMother=Locations("wormMother",
                        ("Moving deeper into the cavern, you come face to face with the large being"
                         "\nyou heard at the entrance. It is actually a giant, bloated worm."
                         "\nSurrounded by countless eggs, you assume that this one gives"
                         "\nbirth to the smaller worms. You notice that the creature has its 'head'"
                         "\npointed away from you. Circling around it, you notice that it is feeding upon"
                         "\nthe core of this smaller vortex, where all of this vortex's mass is heading."
                         "\nIt seems to ignore your presence, as it is preoccupied with consuming the core"
                         "\nbehind it."),
                         "You return to the living space of the great worm. The worms inside the eggs here"
                         "\nare much smaller than those near the entrance.",
                         ["fluids"],
                         8)

    graveyard=Locations("graveyard",
                       ("You approach an area littered with corpses of a different kind. Upon closer inspection,"
                        "\nyou see that each one looks completely bizarre, as well as completely different from"
                        "\nthe others. If you would call them something, they would fit the many ideas you have"
                        "\nof what aliens look like. You wonder why no two corpses are alike."),
                        "You return to the graveyard of aliens.",
                        [],
                        9)

    wormSanctum=Locations("wormSanctum",
                         ("Slipping past the greater worm, you venture deeper into the inner vortex. "
                          "\nAs your visibility decreases, you rely more and more on your hearing to guide you."
                          "\nYou begin to hear wriggling all around you. You soon understand that the noise"
                          "\nis coming from worms. They sound like they are moving closer to you. The worms"
                          "\nsound threatening."),
                          "You return to the worm's sanctum. You manage to keep them at bay with your worm skin",
                          ["heart"],
                          10)

    vicousWorm=Locations("vicousWorm",
                        ("You approach a mist-like cloud, which is being 'attacked' by a worm."
                         "\nTo be accurate, the worm seems to be passing through the mist-like entity,"
                         "\nfruitlessly trying to take a bite out of it. The mist seems unaffected by the"
                         "\nworm, both physically and in terms of attention."),
                        ("You return to where the worm was attacking the mist-like entity. The worm has"
                         "\nyet to fulfill its intentions."),
                         [],
                         11)



    wall=Locations("wall",                                                  #"Fake" location that complies with rest of 'paths' matrix
                  ("Walking too far away from your intended path, you are thrown back by "
                   "\ngale-force winds. Try a different direc tion."),
                  ("Walking too far away from your intended path, you are thrown back by "
                   "\ngale-force winds. Try a different direction."),
                   [],
                   -1)





    mapBasic=("                                                                            Worm Den"
              "\n                                                                               ||    "
              "\nMist Leader========Black Hole Core===============Worm Incubators==========Greater Worm"
              "\n                         ||                             ||"
              "\nLesser Mists=======Worm First Encounter==========Worm Graveyard=======Extraterrestrial Graveyard"
              "\n     ||                  ||                             ||"
              "\nDistorted Zone======Point of Entry             Vicious Mist Worm")



    title= ("============"
            "\n||WORMHOLE||"
            "\n============")


    commands=["north","south","east","west","map","points","examine","take","drop","help","quit","inventory","use"]

    commandsAbb=["n","s","e","w","m","p","x","t","d","h","q","i","u"]


    hasMoved=True

    hasExamined=[False,False,False]

    bWormStatus=True

    heartStatus=False

    paths=[
        [  wormSpace,   wall,          wall,        distortedZone ],
        [  wall,        distortedZone, wormSpace,   wall          ],
        [  vortexCore,  vortexEntry,   morgue,      cloudSpace    ],
        [  innerVortex, vicousWorm,    graveyard,   wormSpace     ],
        [  cloudSpace,  wall,          vortexEntry, wall          ],
        [  wall,        wall,          vortexCore,  wall          ],
        [  wall,        wormSpace,     innerVortex, mistLeader    ],
        [  wall,        morgue,        wormMother,  vortexCore    ],
        [  wormSanctum, wall,          wall,        innerVortex   ],
        [  wall,        wall,          wall,        morgue        ],
        [  wall,        wormMother,    wall,        wall          ],
        [  morgue,      wall,          wall,        wall          ]
                                                                  ]


    itemTaken=[False,False,False]

    score=0

    wrongDirection= ("Walking too far away from your intended path, you are thrown back by "
                     "\ngale-force winds. Try a different direction.")


    creditsInfo="Copyright (c) Owen Burek, 2016 \nOwen.Burek1@marist.edu"


    def displayTitle():
        print(title)
        input("Press ENTER to continue")


    def displayIntro(name):
        intro=("The last thing "+name+" remembers is sitting comfortably inside their living room, "
               "\nwatching television on a Sunday afternoon. \nA moment later, "+name+" suddenly"
               "\nlost consciousness. They awake, only to find themselves floating in what"
               "\nthey can only describe as a cosmic whirlpool.")
        
        print()
        print(intro)
        print()

    def characterName():
        name=input("What is your name? ")
        return name
        
        
    def getDestination(playerLocation, direct,player):
        global destination, score, hasMoved

        if direct>3:
            return player.location
        
        if direct<4:
            destination=paths[playerLocation][direct]
            
            if destination==wall:
                    destination=player.location
                    hasMoved=False
                    barrier()
                    return destination
                
            if not destination==wall:
                if player.location.visited==False:
                    player.location.visited=True
                    player.score+=5
                
            return destination

        
    def displayScene(player,change):
        if change==0:
            print(player.location.descriptionBefore)
            player.location.whichDesc=1
        else:
            print(player.location.descriptionAfter)

    def displayInventory(player):
        if len(player.inventory)==0:
            print("You don't have anything right now.")
        else:
            print("INVENTORY:")
            for i in range(len(player.inventory)):
                print(player.inventory[i])

    def changeLocationDesc():
        if player.location.whichDesc==0:
            displayScene(player,0)
        else:
            displayScene(player,1)
            

    def processDirection():
        global examItem, item
        examItem=0
        print()
        direction=input("Where do you want to go now? ")
        print()
        direction=direction.lower()
        for i in range(len(direction)):
            if direction[i]==" ":
                examItem=1
                itemDirection=direction.split(" ")
                [direction,item]=map(str,itemDirection)
                for i in range (13):
                    if direction==commands[i] or direction==commandsAbb[i]:
                        return i
                
        for i in range (13):
            if direction==commands[i] or direction==commandsAbb[i]:
                return i
        print("Invalid command. Type 'help' for available directions.")
        return processDirection()


    def barrier():
        print(wrongDirection)
        print()


    def gameEnd(name):
        input("Press ENTER to continue")
        ending=(name+"'s body is violently pulled forward into the distorted zone. "
            "\nHurtling onward, "+name+" is ejected into what they understand to be deep space. "
            "\nThis realization doesn't last long, however, as "+name+"'s consciousness fades once more."
            "\nThey know not what claimed their life first: the cold nature of space "
            "itself freezing them, \nor the lack of oxygen suffocating them.")
        print(ending)
        print("GAME OVER")
        input("Press ENTER to continue")


    def displayCredits():
        print(creditsInfo)


    def showHelp():
        print("Available commands: north, west, east, south, help,"
              "\npoints, map, examine, take, drop, quit, inventory, and use")


    def showScore(score):
        print("SCORE:",score)

            
    def showMap(player):
        for i in player.inventory:
            if i=="map":
                print()
                print()
                print(mapBasic)
                print()
                print()
                return True
        print("You don't have a map.")

    def examineArea(hasItem,player):
        if hasItem==False:
            displayScene(player,0)

        else:
            for item in player.location.item:    
                if item=="skin":
                    print("Out of the corner of your eye, you notice a strange worm corpse. (skin)")
                    morgue.hasExamined=True

                    
                elif item=="organs":
                    print("There seems to be a pile of worm organs still inact within a worm that is "
                          "\nin front of the wall of distortion. (organs)")
                    distortedZone.hasExamined=True

                    
                elif item=="map":
                    print("A special worm corpse catches your attention. (map)")
                    mistLeader.hasExamined=True

                   
                elif item=="dew":
                    print("You notice that the mist-like cratures leave a trail of dew floating behind them."
                          "\n(dew)")
                    cloudSpace.hasExamined=True

                   
                elif item=="fluids":
                    print("Seeing the fluids pulse through the huge worm, you wonder if they could be "
                          "\nuseful to you. (fluids)")
                    wormMother.hasExamined=True

                elif item=="heart":
                    print("Looking past the worms, you notice something making a thumping sound. (heart)")
                    wormSanctum.hasExamined=True

               
    def examineAreaItem(hasItem,player,item):
        if hasItem==False:
            print("There's nothing noteworthy around here.")
            
        else:
            if item=="skin":
                print("Amongst the many dried up corpses, you notice one that is about twice as large"
                      "\nas the rest. Upon closer inspection, its skin is rock hard, and it doesn't"
                      "\nsnap when you try to bend it, unlike the rest of them.")
                morgue.hasExamined=True
                
            elif item=="organs":
                print("Close to the wall of distortion, you find a worm that has been pulled in half."
                      "\nIts skin is shriveled and brittle and, upon contact,"
                      "\nit disintegrates into flakes. However, the worm's insides remain."
                      "\nPicking up one of its organs, you can still feel life pulsing through it,"
                      "\ndespite being attached to nothing. Out of surprise, you drop it.")
                distortedZone.hasExamined=True
                
            elif item=="map":
                print("One of the corpses near the huge mist entity seems different than the rest."
                      "\nExamining it, you see that the skin had been sliced on one side so that"
                      "\nthe tube could be flattened into a long sheet of skin. On the sheet are"
                      "\nnumerous carvings that look like interconnected locations. 'Oh, so you've"
                      "\nnoticed that. The one who made that was in the same position you are in now,"
                      "\nwith no home to return to. Having devoured the species of that lone survivor,"
                      "\nI can understand his language. I'll translate it into something you can"
                      "\nunderstand.' The mist entity explains each of the symbols, which correspond"
                      "\nto various areas.")
                mistLeader.hasExamined=True

            elif item=="dew":
                print("You notice the dew trailing behind the misty clouds. As you come into contact "
                      "\nwith a drop of dew, it sticks to your clothes.")
                cloudSpace.hasExamined=True

            elif item=="fluids":
                print("As you observe the greater worm, you notice as it quivers slightly. From its"
                      "\nback an egg is released, along with a great deal of fluid of which you are"
                      "\ncovered in. It seems viscous enough to be gathered up.")
                wormMother.hasExamined=True

            elif item=="heart":
                print("You dash into the swarm of worms and notice a heart, nearly getting yourself"
                      "\ndevoured in the process.")
                wormSanctum.hasExamined=True

            else:
                print("There's nothing like that around here.")
                


    def takeItem(hasItem,player,item):
        if len(player.location.item)==0:
            print("There's nothing around here I can take.")
            
            
        else:
            for i in player.location.item:
                if i=="skin" and item=="skin":
                    print("You pick up the large corpse. It feels weightless.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                elif i=="organs" and item=="organs":
                    print("You gingerly pick the organ back up. At least its not slimy.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                elif i=="map" and item=="map":
                    print("You take the worm skin map with you.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                elif i=="dew" and item=="dew":
                    print("You stick a handful of dew drops to your hand.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                elif i=="fluids" and item=="fluids":
                    print("You gather up the fluids into a single viscous orb and take it with you.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                elif i=="heart" and item=="heart":
                    print("You quickly grab the heart and retreat to safety.")
                    player.inventory.append(item)
                    player.location.item.remove(item)
                else:
                    print("There's no item like that here.")
            
            


    def dropItem(item,player):
        
        for i in player.inventory:    
            if i=="skin" and item=="skin":
                print("You drop the hardened worm skin.")
                player.location.item.append(item)
                player.inventory.remove(item)
            elif i=="organs" and item=="organs":
                print("You gently let go of the organs.")
                player.location.item.append(item)
                player.inventory.remove(item)
            elif i=="map" and item=="map":
                print("You roll up your map and drop it.")
                player.location.item.append(item)
                player.inventory.remove(item)
            elif i=="dew" and item=="dew":
                print("You somehow manage the scrape the dew off you.")
                player.location.item.append(item)
                player.inventory.remove(item)
            elif i=="fluids" and item=="fluids":
                print("You lay the orb of fluid down. The surface tension keeps the orb from spilling.")
                player.location.item.append(item)
                player.inventory.remove(item)
            elif i=="heart" and item=="heart":
                print("You gingerly place the heart on the ground. It shivers with each beat.")
                player.location.item.append(item)
                player.inventory.remove(item)
            else:
                print("You don't have anything like that to drop.")

    def goodEnd(alt):
        if alt==1:
            input("PRESS ENTER TO CONTINUE")
            print("Once more, the huge mist's voice permeates through your mind: 'It appears that you are"
                  "\nprepared to ascend to my plane of being. Drink the fluids of that breeding worm, and"
                  "\ncover youself with the dew of my lesser kind.' You think about the mist's proposal."
                  "\nIt did not seem just to let the mist continue to destroy planets for its own gain."
                  "\nThinking about how you could stop it, you wonder what the items you have could do."
                  "\nIf the greater worm fluids could bring you a renewed, superior life, maybe it could"
                  "\nalso restore the life of the deceased. You take out the organs and worm skin you've"
                  "\nbeen carrying around. Shoving the organs inside the husk along with the greater worm's"
                  "\nbodily fluids, you patiently wait to see if your plan will bear any fruit.'Do you"
                  "\nhonestly think that such a primitave creature such as yourself can even damage me? You'll"
                  "\nnever understand the true extent of my power until you become like me.'"
                  "\nThe dead worm suddenly comes back to life, wriggling about manically. There's only one"
                  "\nmore step. You spread the dew stuck to you on the worm. It begins to fade until it "
                  "\nresembles a mist cloud in its former shape. Hungrily, the worm leaps at you,"
                  "\nbut merely passes through you. 'I guess I underestimated your ability to think"
                  "\ncritically. I did not expect you to suspect me of lying to you. But you are correct,"
                  "\nI planned to kill you once you were like me and move on to another planet.' As the mist,"
                  "\nunveils its plans to you, the worm, finding a new target, leaps towards the greater mist."
                  "\nAs it comes into contact with it, the worm consumes each drop of its dew, "
                  "\nleaving little behind. 'Ahh... so you were trying to have this thing consume me all"
                  "\nalong? I have definetely understimated your conviction to take me down, human. "
                  "\nHowever, you must surely realize that my death will not do anything to save you. In"
                  "\nfact, it only assures that you will die here, too.'"
                  "\nYou assumed as much. But, at least by killing the mist, you retained your human values"
                  "\nup until the end. Knowing that the mist would never understand your sentimentality,"
                  "\nyou close your eyes, and await a quick death.")
            print("GAME OVER")
            input("Press ENTER to continue")

        if alt==2:
            input("PRESS ENTER TO CONTINUE")
            print("Approaching the huge mist entity, its tone seems apprehensive: 'You are not the same"
                  "\nas you were before. You seem far less human, not to mention that growth"
                  "\nin your chest. What have you done?' Ignoring the voice, "
                  "\nyou experience an incredibly sharp pain in your stomach. You are comsumed by a "
                  "\ndesire to satisfy your hunger, as you lumber towards the huge entity. Contrary "
                  "\nto your expectations, taking a bite of the mist is satisfying. As you continue to"
                  "\ndo so, new thoughts you cannot truly comprehend fill your mind. You also "
                  "\nacknowledge that your body is becoming more like the mist-like entity with every"
                  "\nbite. 'What is this!? Have you no loyalty towards your species? How much malice"
                  "\nlies in your heart, mortal?'"
                  "\n..."
                  "\n'Fine then, become not my partner, but my successor! Consume species in my stead,"
                  "\nand satisfy your hunger!' The voice, at last, fades. Now with power beyond "
                  "\nyour imagination, you can continue where that wretched creature left off, and "
                  "\nconsume to your heart's content.")
            print("GAME OVER")
            input("PRESS ENTER TO CONTINUE")
            

    def lifeOrDeath(player):
        for i in player.inventory:
            if i=="skin":
                print("The worms attack you en masse, but they recoil at the sight of the hardened"
                      "\nskin of their own kind. They keep their distance, yet remain alert.")
                return True
        input("Press ENTER to continue")
        print("The worms attack you en masse and quickly overpower you with sheer numbers. "
              "\nEnveloped under a wriggling mass of them, you feel your life slip away."
              "\nGAME OVER")
        input("Press ENTER to continue")
        return False

    def useItem(item,player):
        global bWormStatus
        global heartStatus
        
        for i in player.inventory:
            if item=="heart":
                print("You let the veins and arteries wrap themselves around you. You are apprehensive"
                      "\nas to what may happen, but you steel yourself and let it do as it pleases."
                      "\nYou feel the tendrils penetrate your chest, and you feel an acute pain in"
                      "\nthe same area. The heart embeds itself in your chest. You begin to develop"
                      "\nan acute desire to eat something.")
                player.inventory.remove("heart")
                heartStatus=True
                return True
        
            elif item=="skin":
                if player.location==wormMother and bWormStatus==True:
                    print("You put all of your weight behind the worm skin and swimg at the massive worm."
                          "\nSurprisingly, a huge wound opens up on its stomach, and it screams in pain."
                          "\nWatching from a safe distance, you wait until the worm stops moving."
                          "\nFluids flow out of its corpse.")
                    bWormStatus=False
                    
                else:
                    print("Swinging that around here won't accomplish anything.")
                return True
            
            print("You can't use that for anything right now.")
        print("No such item exists.")
        
    def main():
        global hasMoved, examItem, item, heartStatus
        
        displayTitle()
        name=characterName()
        player=Player(name,vortexEntry)
        displayIntro(player.name)
        
        player.location=vortexEntry
        vortexEntry.visited=True
        destination=0
        
        displayScene(player,0)

        while True:
            
            if len(player.inventory)==6 and player.location==mistLeader:
                goodEnd(1)
                print()
                break

            if player.location==wormSanctum:
                fate=lifeOrDeath(player)
                if fate==True:
                    pass
                else:
                    break
            if heartStatus==True and player.location==mistLeader:
                goodEnd(2)
                print()
                break
                    
            if player.moves>25:
                gameEnd(name)
                print()
                break

            direction=processDirection()
            
            player.location=getDestination(player.location.num,direction,player)
            
            if direction==4:    
                showMap(player)
                hasMoved=False
                
            elif direction==5:
                showScore(player.score)
                hasMoved=False
                
            elif direction==6:        
                if examItem==0:
                    if len(player.location.item)==0:
                        examineArea(False,player)
                    else:
                        examineArea(True,player)
                else:
                    if len(player.location.item)==0:
                        examineAreaItem(False,player,item)
                    else:
                        examineAreaItem(True,player,item)
                hasMoved=False
            
            elif direction==7:
                if examItem==0:
                    print("Please enter the item name after the 'take' command you wish to take.")
                else:
                    if len(player.location.item)==0:
                        takeItem(False,player,item)
                    else:
                        takeItem(True,player,item)
                hasMoved=False
                
            elif direction==8:
                if examItem==0:
                    print("Please enter the item name after the 'drop' command you wish to drop.")
                else:        
                    dropItem(item,player)
                hasMoved=False

            elif direction==9:
                showHelp()
                hasMoved=False
                
            elif direction==10:
                break

            elif direction==11:
                displayInventory(player)
                hasMoved=False

            elif direction==12:
                if examItem==0:
                    print("Please enter the item name after the 'use' command you wish to use.")
                else:
                    useItem(item,player)
                hasMoved=False
                          
            if hasMoved:
                displayScene(player,player.location.whichDesc)
                player.moves+=1
            else:
                hasMoved=True
                
                    
        print()
        displayCredits()
        
    

    main()

    answer=input("Do you want to play again?")
    answer=answer.lower()
    if answer=="yes" or answer=="y":
        print()
        continue
    else:
        break

